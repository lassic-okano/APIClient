//
//  SJWeather.swift
//  APIClient
//
//  Created by ryookano on 2018/01/05.
//  Copyright © 2018年 ryookano. All rights reserved.
//

import UIKit
import SwiftyJSON

class SJWeather: NSObject {
    var title: String = ""
    var link: String = ""
    var forecasts: [SJForecast]?
    var location: SJWeatherLocation?
    var desc: SJWeatherDescription?

    init? (object: JSON) {
        self.link = object["link"].stringValue
        self.title = object["title"].stringValue
        self.location = SJWeatherLocation(object: object["location"])!
        self.desc = SJWeatherDescription(object: object["description"])!
        self.forecasts = [SJForecast(object: object["forecasts"])!] // :TODO
    }

    override init() {
        title = ""
        link = ""
    }
}

class SJWeatherLocation: NSObject {
    var city: String = ""
    var area: String = ""
    var prefecture: String = ""

    init? (object: JSON) {
        self.city = object["city"].stringValue
        self.area = object["area"].stringValue
        self.prefecture = object["prefecture"].stringValue
    }
}

class SJWeatherDescription: NSObject {
    var text: String
    var publicTime: String

    init? (object: JSON) {
        self.text = object["text"].stringValue
        self.publicTime = object["publicTime"].stringValue
    }
}

class SJForecast: NSObject {
    var dateLabel: String
    var telop: String
    var date: String
    var temperature: SJTemperatureCollection
    var image: SJWeatherImage

    init? (object: JSON) {
        self.dateLabel = object["dateLabel"].stringValue
        self.telop = object["telop"].stringValue
        self.date = object["date"].stringValue
        self.temperature = SJTemperatureCollection(object: object["temperature"])!
        self.image = SJWeatherImage(object: object["image"])!
    }
}

class SJTemperatureCollection: NSObject {
    var min: SJTemperature?
    var max: SJTemperature?
    init? (object: JSON) {
        self.min = SJTemperature(object: object["min"])!
        self.max = SJTemperature(object: object["mix"])!
    }
}

class SJTemperature: NSObject {
    var celsius: String
    var fahrenheit: String
    init? (object: JSON) {
        self.celsius = object["celsius"].stringValue
        self.fahrenheit = object["fahrenheit"].stringValue
    }
}

class SJWeatherImage: NSObject {
    var width: Int
    var height: Int
    var title: String
    var url: String
    init? (object: JSON) {
        self.width = object["width"].intValue
        self.height = object["height"].intValue
        self.title = object["rirle"].stringValue
        self.url = object["url"].stringValue
    }
}
