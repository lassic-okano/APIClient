//
//  ViewController.swift
//  APIClient
//
//  Created by ryookano on 2018/01/04.
//  Copyright © 2018年 ryookano. All rights reserved.
//

import UIKit
import APIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

    let domain              = "http://weather.livedoor.com/forecast/webservice/json"
    let path                = "/v1"
    let tottori             = 310010
    let tokyo               = 130010

    @IBOutlet weak var mainTitle: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var area: UILabel!
    @IBOutlet weak var text: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clear()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // action
    @IBAction func apikitButtonHandler(_ sender: Any) {
        requestApiKit()
    }
    @IBAction func alamofireButtonHandler(_ sender: Any) {
        requestAlamofire()
    }

    // APIKit + codable
    // :TODO keyがないとfailureになる
    func requestApiKit() {
        clear()
        let request = WeatherRequest(cityCode: tottori)
        Session.send(request) { result in
            switch result {
            case .success(let response):
                self.setWeather(weather: response)
                break
            case .failure(let error):
                print(error.localizedDescription)
                break
            }
        }
    }

    // Alamofire + SwiftyJSON
    func requestAlamofire() {
        clear()
        Alamofire.request(domain + path, parameters: ["city": tokyo]).responseJSON { response in
            if (response.result.isSuccess) {
                let json = JSON(response.result.value!)
                let weather = SJWeather(object: json)
                self.setWeather(weather: weather!)
            } else {
                print(response.error!)
            }
        }
    }

    // set codable
    func setWeather(weather: WeatherNews) {
        mainTitle.text = weather.title
        city.text = weather.location.city
        area.text = weather.location.area
        text.text = weather.description.text
    }

    // set SwiftyJSON
    func setWeather(weather: SJWeather) {
        mainTitle.text = weather.title
        city.text = weather.location?.city
        area.text = weather.location?.area
        text.text = weather.desc?.text
    }

    func clear() {
        mainTitle.text = ""
        city.text = ""
        area.text = ""
        text.text = ""
    }
}

