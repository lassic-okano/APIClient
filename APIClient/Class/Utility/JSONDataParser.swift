//
//  JSONDataParser.swift
//  APIClient
//
//  Created by ryookano on 2018/01/04.
//  Copyright © 2018年 ryookano. All rights reserved.
//

import UIKit
import APIKit

class JSONDataParser: APIKit.DataParser {
    var contentType: String? {
        return "application/json"
    }

    func parse(data: Data) throws -> Any {
        // ここではデコードせずにそのまま返す
        return data
    }
}
